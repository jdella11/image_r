package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.database


import android.content.Context
import androidx.room.Room
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.IUriRepository
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.UriData

class UriDataDBRepo (ctx : Context) : IUriRepository{
    //Room database object for UriData database

    private val UriDataList: MutableList<UriData> = mutableListOf()
    private val db: UriDataDB

    init {
        db = Room.databaseBuilder(
            ctx,
            UriDataDB::class.java,
            "uriData.db"
        ).allowMainThreadQueries().build()
    }

    //Flips list around so it displays entries in the opposite order of newest to oldest rather than the other way around.
    private fun refreshUriDataList() {
        UriDataList.clear()
        val uriDataList = db.uridataDAO().getAllUriData()
        UriDataList.addAll(uriDataList)
        UriDataList.reverse()
    }

    override fun getSize(): Int {
        return UriDataList.size
    }

    override fun getUriData(index: Int): UriData {
        return UriDataList[index]
    }

    override fun getUriDataList(): List<UriData> {
        if(UriDataList.size == 0){
            refreshUriDataList()
        }
        return UriDataList
    }

    override fun removeUriData(uriData: UriData) {
        db.uridataDAO().deleteUriData(uriData)
        refreshUriDataList()
    }

    override fun removeAllUriData() {
        db.uridataDAO().purgeAll()
        refreshUriDataList()
    }

    override fun addUriData(uriData: UriData) {
        db.uridataDAO().addUriData(uriData)
        refreshUriDataList()
    }

    override fun replaceUriData(idx: Int, uriData: UriData) {
        db.uridataDAO().updateUriData(uriData)
    }


}

