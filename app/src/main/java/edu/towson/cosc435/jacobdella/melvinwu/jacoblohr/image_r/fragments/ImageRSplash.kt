package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.MainActivity
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.R
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.adapters.ImageRRecylerAdapter
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.IImageRController
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.UriData
import kotlinx.android.synthetic.main.fragment_image_r_splash.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.internal.wait
import java.util.*


/**
 * This is going to house the fragment for the splash screen when you open the app.
 */
class ImageRSplash : Fragment() {
    //Instantiate our controller.
    private lateinit var imageRcontroller: IImageRController

    //Check if our controller is of the right type.
    override fun onAttach(context: Context) {
        super.onAttach(context)
        when (context) {
            is IImageRController -> imageRcontroller = context
            else -> throw Exception("IImage_RController Expected")
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_r_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = ImageRRecylerAdapter(imageRcontroller)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        openSettingsBtn?.setOnClickListener { imageRcontroller.openSettingsPage() }
        loadImageBtn?.setOnClickListener { selectImageInAlbum() }
        removeAllRecents.setOnClickListener { RemoveAllRecentImages() }
        startImgDownload.setOnClickListener{imageRcontroller.startImageDownloadService()}
        quoteOfTheDay.visibility = View.INVISIBLE
        UpdateRandomQuote()
        checkWritePermissions()
    }


    //Select image from an albums
    //Grabs an intent
    private fun selectImageInAlbum() {
        val intent =
            Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.type = "image/*"
        if (activity?.packageManager?.let { intent.resolveActivity(it) } != null) {
            startActivityForResult(intent, REQUEST_SELECT_IMAGE_IN_ALBUM)
        }
    }

    companion object {
        private val REQUEST_SELECT_IMAGE_IN_ALBUM = 1
    }

    // On activity result we dump the intent into a bitmap
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        var imageUri = data?.data
        if (imageUri != null) {
            addUri(imageUri)
        }
    }

    // Onresume we refresh the recycler view and grab bitmap data from the database.
    override fun onResume() {
        super.onResume()
        imageRcontroller.uriDataRepo.getUriDataList()
        imageRcontroller.settingsDataRepo.getSettingsList()
        recyclerView.adapter?.notifyDataSetChanged()
    }

    private fun addUri(uri: Uri) {
        imageRcontroller.addUriData(UriData(UUID.randomUUID(), uri, Date().toString()))
    }
    //Removes all recent images from the database
    private fun RemoveAllRecentImages() {
        if (imageRcontroller.uriDataRepo.getSize() != 0) {
            var dialog: AlertDialog?
            val builder = view?.context?.let { AlertDialog.Builder(it) }
            val dialogClickListener = DialogInterface.OnClickListener { _, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        imageRcontroller.uriDataRepo.removeAllUriData()
                        imageRcontroller.uriDataRepo.getUriDataList()
                        recyclerView.adapter?.notifyDataSetChanged()
                        Toast.makeText(
                            view?.context,
                            "All recent images have been removed",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    DialogInterface.BUTTON_NEGATIVE -> Toast.makeText(
                        view?.context,
                        "Recent images have not been removed.",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }
            builder?.setTitle("Are you sure you want to remove all " + imageRcontroller.uriDataRepo.getSize() + " recent images?")
            builder?.setMessage("Please select an option")
            builder?.setPositiveButton("YES", dialogClickListener)
            builder?.setNegativeButton("NO", dialogClickListener)
            dialog = builder?.create()
            dialog?.show()
        } else {
            Toast.makeText(
                view?.context,
                "No recent images",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    //Fetches a random quote from the API data.
    @SuppressLint("SetTextI18n")
    fun UpdateRandomQuote() {
        try {
            imageRcontroller.launch(Dispatchers.Main) {
                delay(400)
                try {
                    if (imageRcontroller.getSettingsData(0).isChecked) {
                        var rand = ((0..imageRcontroller.quotesRepo.size).random()) - 1
                        quoteOfTheDay.text =
                            imageRcontroller.quotesRepo[rand].quote_author + " - " + imageRcontroller.quotesRepo[rand].quote_content
                        quoteOfTheDay.visibility = View.VISIBLE
                    } else {
                        quoteOfTheDay.text = ""
                        quoteOfTheDay.visibility = View.VISIBLE
                    }
                } catch (e: Exception) {
                    quoteOfTheDay.text = ""
                    quoteOfTheDay.visibility = View.VISIBLE
                }

            }

        } catch (e: java.lang.Exception) {
            quoteOfTheDay.text = ""
            quoteOfTheDay.visibility = View.VISIBLE
        }
    }

    //Checks write permissions as they are needed to write the SD Card
    private fun checkWritePermissions() {
        if (ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity!!, arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 0
            )
            ActivityCompat.requestPermissions(
                activity!!, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), 1
            )
        } else {
            Log.e("DB", "PERMISSION GRANTED")
        }

    }

}



