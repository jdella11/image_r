package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.adapters


import android.content.DialogInterface
import android.graphics.Bitmap
import android.provider.MediaStore
import android.provider.MediaStore.Images.Media.getBitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.R
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.IImageRController
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.UriData
import kotlinx.android.synthetic.main.image_r_recent_view_recycler.view.*
import kotlinx.coroutines.launch

//The recycler adapter class.
class ImageRRecylerAdapter(private val imageRcontroller: IImageRController) : RecyclerView.Adapter<RecentImageViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentImageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.image_r_recent_view_recycler, parent, false)
        val viewHolder = RecentImageViewHolder(view)

        view.recyclerLayout.setOnLongClickListener() {
            //Dialog for deleting a picture from the recyclerview
            val position = viewHolder.adapterPosition
            lateinit var dialog: AlertDialog
            val builder = AlertDialog.Builder(view.context)
            val dialogClickListener = DialogInterface.OnClickListener{ _, which ->
                when(which){
                    DialogInterface.BUTTON_POSITIVE -> {
                            imageRcontroller.removeUriData(position)
                            this.notifyItemRemoved(position)
                            Toast.makeText(view.context,  "Image number " + position +" has been removed.", Toast.LENGTH_SHORT).show()
                    }
                    DialogInterface.BUTTON_NEGATIVE -> Toast.makeText(view.context,  "Image number " + position + " has not been removed.", Toast.LENGTH_SHORT).show()

                }
            }
            builder.setTitle("Do you want to delete image number " + (position+1) +"?")
            builder.setMessage("Please select an option")
            builder.setPositiveButton("YES",dialogClickListener)
            builder.setNegativeButton("NO",dialogClickListener)
            dialog = builder.create()
            dialog.show()
            false
        }

        // Grab the bitmap from the viewholder and change the edit bitmap on our main controller
        // We launch a new view with the bitmap being set.
        view.recyclerLayout.editImageBtn.setOnClickListener {
            val position = viewHolder.adapterPosition
            imageRcontroller.launch {
                imageRcontroller.setEditBitmap(imageRcontroller.getUriData(position).uri)
            }
            imageRcontroller.editPhotoPage()
        }



        return viewHolder
    }

    override fun getItemCount(): Int {
        return imageRcontroller.getUriDataSize()
    }

    override fun onBindViewHolder(holder: RecentImageViewHolder, position: Int) {
        val uriData = imageRcontroller.getUriData(position)
        holder.bindUriData(uriData)
    }





}

// Spawns a viewholder
class RecentImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bindUriData(uriData: UriData) {
        itemView.imageViewRecycler.setImageURI(uriData.uri)
        itemView.dateOpened.text = uriData.date
    }
}



