package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models


//Contains Data for the quotes
data class QuoteData(
    var quote_id : Int,
    var quote_content : String,
    var quote_author : String
)
