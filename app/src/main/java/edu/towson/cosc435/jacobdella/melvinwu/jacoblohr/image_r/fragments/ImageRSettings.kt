package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.MainActivity

import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.R
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.IImageRController
import kotlinx.android.synthetic.main.fragment_image_r_settings.*
import kotlinx.coroutines.launch

/**
 * This is the settings for the app, mainly just gonna use to edit some crap in the database.
 */
class ImageRSettings : Fragment() {
    //Instantiate our controller.
    private lateinit var imageRcontroller : IImageRController

    //Check if our controller is of the right type.
    override fun onAttach(context: Context) {
        super.onAttach(context)
        when (context) {
            is IImageRController -> imageRcontroller = context
            else -> throw Exception("IImage_RController Expected")
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_r_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try{
            imageRcontroller.settingsDataRepo.getSettingsList()
        } catch (e : Exception) {
            Log.e(MainActivity.TAG, "Error : ${e.message}")
        }

        //Every time a switch changes state we update the database
        networkSwitch.isChecked = imageRcontroller.getSettingsData(0).isChecked
        networkSwitch.setOnCheckedChangeListener { _, isChecked ->
            imageRcontroller.toggleSettingData(0)
        }
        //Every time a switch changes state we update the database
        purgeSwitch.isChecked = imageRcontroller.getSettingsData(1).isChecked
        purgeSwitch.setOnCheckedChangeListener { _, isChecked ->
            imageRcontroller.toggleSettingData(1)
        }
        //Every time a switch changes state we update the database
        resetSwitch.isChecked = imageRcontroller.getSettingsData(2).isChecked
        resetSwitch.setOnCheckedChangeListener { _, isChecked ->
            imageRcontroller.toggleSettingData(2)
        }
    }

    override fun onResume() {
        super.onResume()
    }


}
