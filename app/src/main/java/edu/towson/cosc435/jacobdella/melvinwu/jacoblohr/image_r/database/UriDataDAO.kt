package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.database

import android.net.Uri
import androidx.room.*
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.UriData
import java.util.*

//UriData Data Access Object
@Dao
interface UriDataDAO {
    @Insert
    fun addUriData(uriData: UriData)

    @Update
    fun updateUriData(uriData: UriData)

    @Delete
    fun deleteUriData(uriData: UriData)

    @Query("SELECT * FROM UriData")
    fun getAllUriData(): List<UriData>

    //Deletes everything from room db
    @Query("DELETE FROM UriData")
    fun purgeAll()
}

class UriConverters {
    @TypeConverter
    //Requires a minimum api to work.
    fun toString(uuid : UUID) : String{
        return uuid.toString()
    }

    @TypeConverter
    //Requires a minimum api to work.
    fun toUUID(uuid: String) : UUID{
        return UUID.fromString(uuid)
}


    @TypeConverter
    //Requires a minimum api to work.
    fun toString(uri : Uri) : String{
        return uri.toString()
    }

    @TypeConverter
    //Requires a minimum api to work.
    fun toUri(uri : String) : Uri{
        return Uri.parse(uri)
    }
}

@Database(entities = [UriData::class], version = 2, exportSchema = false)
@TypeConverters(UriConverters::class)
abstract class UriDataDB : RoomDatabase() {
    abstract fun uridataDAO(): UriDataDAO
}

