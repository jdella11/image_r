package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.aggregators

import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.IUriRepository
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.UriData


//Stores UriData data.
class UriDataRepository : IUriRepository {
    private var uridataRepo : MutableList<UriData> = mutableListOf()

    override fun getSize(): Int {
        return uridataRepo.size
    }

    override fun removeAllUriData(){
        uridataRepo.clear()
    }

    override fun addUriData(uriData: UriData) {
        uridataRepo.add(uriData)
    }
    override fun getUriData(index: Int): UriData {
        return uridataRepo[index]
    }

    override fun getUriDataList(): List<UriData> {
        return uridataRepo
    }

    override fun removeUriData(uriData: UriData) {
        uridataRepo.remove(uriData)
    }

    override fun replaceUriData(idx: Int, uriData: UriData) {
        if(idx >= uridataRepo.size) throw Exception("Outside of bounds")
        uridataRepo[idx] = uriData
    }



}