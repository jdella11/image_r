package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models

import android.graphics.Bitmap
import android.net.Uri
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

//Contains data for the UriData also a room entity
@Entity
data class UriData (
    @PrimaryKey
    val uuid : UUID,
    @ColumnInfo(name = "uri")
    var uri: Uri,
    @ColumnInfo(name = "date")
    var date : String

)
