package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.MainActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*
import kotlin.coroutines.CoroutineContext

class ImageDownloadService : JobService(), CoroutineScope{
    private val client: OkHttpClient = OkHttpClient()
    private var hasDownloaded : Boolean = false
    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    override fun onCreate(){
        super.onCreate()
        CreateNotificationChannel()
    }

    //Iterates through a specific range of URLS and downloads the photo to SD Card.
    override fun onStartJob(params: JobParameters?): Boolean {
        launch(Dispatchers.IO) {
            for(x in 1..5){
                var id = 90 + x
                var FETCH_URL = "https://i.picsum.photos/id/" + id + "/200/300.jpg"
                if(checkFiles(FETCH_URL) == null) {
                    var request = Request.Builder()
                        .url(FETCH_URL)
                        .get()
                        .build()
                    var stream = client.newCall(request).execute().body?.byteStream()
                    var buffer = BitmapFactory.decodeStream(stream)
                    saveFile(FETCH_URL, buffer)
                }
            }
        }

        //Unimplemented Broadcast Receiver
        val intent = Intent()
        intent.action = "MainActivity.ACTION_OK"
        intent.setPackage("edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.MainActivity")
        intent.putExtra("STATUSCODE", 200)
        sendBroadcast(intent)

        //Creates a notification
        val notif = createNotfication()
        NotificationManagerCompat.from(this@ImageDownloadService).notify(NOTIF_ID,notif)
        return true
    }

    private fun CreateNotificationChannel(){
        //Creates a notification channel if API is right
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name = "Image download channel"
            val desc = "Images downloaded!"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID , name,importance).apply {
                description = desc
            }

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    //Creates a notification
    private fun createNotfication() : Notification {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        return NotificationCompat.Builder(this, CHANNEL_ID) .apply {
            setContentIntent(pendingIntent)
        }
            .setContentText("Image downloaded to storage")
            .setContentTitle("Image_R App")
            .setSmallIcon(android.R.drawable.btn_default_small)
            .setAutoCancel(true)
            .build()
    }

    //Write images to SD Card
    private fun saveFile(url: String, bitmap: Bitmap?) {
        if(bitmap != null) {
            getLast2PathSegmentsAsString(url)?.let { filename ->
                saveImageToExternalStorage(bitmap)
            }
        }
    }
    //Parses file name
    private fun getLast2PathSegmentsAsString(url: String) : String? {
        val segments = Uri.parse(url)?.pathSegments
        val last2 = segments?.subList(segments.size - 2, segments.size)
        return last2?.reduce { acc, s -> acc + s }
    }

    //Checks to see if pictures have already been downloaded
    private fun checkFiles(url: String): Bitmap? {
        getLast2PathSegmentsAsString(url)?.let { filename ->
            val saveFile = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), filename)
            if(saveFile.exists()) {
                return BitmapFactory.decodeStream(saveFile.inputStream())
            } else {
                return null
            }
        }
        return null
    }


    private fun saveImageToExternalStorage(bitmap:Bitmap?) {
        // Get the external storage directory path
        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!path.exists()) {
            path.mkdirs()
        }
        // Create a file to save the image
        val file = File(path, "${UUID.randomUUID()}.jpg")
        try {
            // Get the file output stream
            val stream: OutputStream = FileOutputStream(file)
            // Compress the bitmap
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            // Flush the output stream
            stream.flush()
            // Close the output stream
            stream.close()

        } catch (e: IOException) { // Catch the exception
            e.printStackTrace()
        }
    }

    companion object {
        val TAG = ImageDownloadService::class.java.simpleName
        val CHANNEL_ID = "ImageRDownload"
        val NOTIF_ID = 1
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO
}