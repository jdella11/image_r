package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.networking

import android.util.Log
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.MainActivity.Companion.TAG
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.IImageRController
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.QuoteData
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import java.lang.Exception
import java.util.*

interface IImagerRAPI {
    suspend fun fetchQuotesAsync() : Deferred<List<QuoteData>>
}

class ImageR_API (val controller : IImageRController) : IImagerRAPI {
    private val FETCH_URL = "https://api.jsonbin.io/b/5eb56df4a47fdd6af15fb92c"
    private val SECRET_KEY = "\$2b\$10\$MHRSGH9Nf1VU6xm3l7s63eNu5rM5Q8Q6XMyl/Pw8prN09VmCB.fFO"
    private val client: OkHttpClient = OkHttpClient()

    override suspend fun fetchQuotesAsync(): Deferred<List<QuoteData>> {
        //Async controller fetch
            return controller.async(Dispatchers.IO) {
                try {
                    val request = Request.Builder()
                        //Have to use a header to access private JSON bin.
                        .header("secret-key", SECRET_KEY)
                        .url(FETCH_URL)
                        .get()
                        .build()
                    val result: String? = client.newCall(request).execute().body?.string()
                    //Parses json into quotes
                    val quotes = parseJson(result)
                    quotes
                } catch (e : Exception) {
                    Log.e(TAG, "Error : ${e.message}")
                    //If something goes wrong we just display this quote up top.
                    var quotes = mutableListOf<QuoteData>()
                    quotes.add(QuoteData(0, "Something went wrong fetching quotes", ""))
                    quotes
                }

            }

        }
    //Iterate through the JSON and parse it into a mutable list.
    private fun parseJson(json: String?): List<QuoteData> {
        try {
            val Quotes = mutableListOf<QuoteData>()
            if (json == null) return Quotes
            val jsonArray = JSONArray(json)
            var idx = 0
            while (idx < jsonArray.length()) {
                var jsonObj = jsonArray.getJSONObject(idx)
                val Quote = QuoteData(
                    quote_id = jsonObj.getString("quote_id").toInt(),
                    quote_content = jsonObj.getString("quote_contents"),
                    quote_author = jsonObj.getString("quote_author")
                )
                Quotes.add(Quote)
                idx++
            }
            return Quotes
        } catch (e : Exception){
            //If this fails we return an exception and return an error quote
            var quotes = mutableListOf<QuoteData>()
            quotes.add(QuoteData(0, "Something went wrong fetching quotes", ""))
            Log.e(TAG, "Error : ${e.message}")
            throw e
            return quotes
        }

    }
}