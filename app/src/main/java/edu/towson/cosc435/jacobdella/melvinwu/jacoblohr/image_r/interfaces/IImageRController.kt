package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces

import android.graphics.Bitmap
import android.net.Uri
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.QuoteData
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.SettingsData
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.UriData
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.networking.IImagerRAPI
import kotlinx.coroutines.CoroutineScope

/**
 * This is the interface to display and control the images, we implement these in the main activity.
 */

interface IImageRController : CoroutineScope {
    fun startImageDownloadService()
    fun resetImage()
    suspend fun fetchQuotesList(): List<QuoteData>
    fun toggleSettingData(idx: Int)
    fun getSettingsData(idx: Int) : SettingsData
    fun getUriData(idx : Int) : UriData
    fun addUriData(uridata: UriData)
    fun removeUriData(idx: Int)
    suspend fun setEditBitmap(uri: Uri)
    fun openSettingsPage()
    fun editPhotoPage()
    fun getUriDataSize(): Int
    var bitmapToEdit: Bitmap?
    var bitmapBase: Bitmap?
    var uriDataRepo : IUriRepository
    var settingsDataRepo : ISettingsRepository
    var quotesRepo : MutableList<QuoteData>
    var imagerAPI : IImagerRAPI
}