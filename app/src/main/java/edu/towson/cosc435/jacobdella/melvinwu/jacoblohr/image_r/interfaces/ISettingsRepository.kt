package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces

import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.SettingsData
/**
 * This is the interface to control the settings.
 */
interface ISettingsRepository{
    fun getSettingsList(): List<SettingsData>
    fun resetAllSettings()
    fun getSettingsData(idx: Int) : SettingsData
    fun replaceSettingsValue(idx : Int, settingsData: SettingsData)
    fun addSettingsData(settingsData: SettingsData)
    fun getSize(): Int
}


