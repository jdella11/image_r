package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r

import android.graphics.*
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.QuoteData
import junit.framework.Assert
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Ignore
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.lang.Exception
import java.util.*


class ImageRInstrumentedUnitTests {
    @Test
    fun resizeBitmap(){
        var bitmap = getBitmap()
        bitmap = bitmap.resize(200,200)
        assertEquals(400, bitmap.width + bitmap.width)
    }

    private fun getBitmap() : Bitmap{
        var bitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888)
        var canvas = Canvas(bitmap)
        var paint = Paint()
        paint.color = Color.RED
        canvas.drawRect(10F, 10F, 100F, 100F, paint)
        return bitmap
    }

    private fun Bitmap.resize(XRes : Int, YRes : Int) : Bitmap {
        return Bitmap.createScaledBitmap(this, 1 * XRes, 1 * YRes, true)
    }

    @Test
    fun rotateBitmap(){
        var bitmap = getBitmap()
        var isSame = false
        bitmap = bitmap.rotate(-90F)
        if(bitmap != bitmap.rotate(270F)){
            isSame = true
        }

        assertEquals(true, isSame)
    }

    @Test
    fun bwconversionBitmap(){
        var bitmap = getBitmap()
        if(bitmap != bitmap.monochrome()){
            assertEquals(true, true)
        } else {
           assertEquals(true, false)
        }
    }


    @Test
    fun changeBitmapColor(){
        var bitmap = getBitmap()
        if(bitmap != changeBitmapColor(bitmap, 9990)){
            assertEquals(true, true)
        } else {
            assertEquals(true, false)
        }
    }



    @Test
    fun changeBitmapColorSpace(){
        var bitmap = getBitmap()
        if(bitmap != changeBitmapColorSpace(bitmap, Bitmap.Config.ALPHA_8)){
            assertEquals(true, true)
        } else {
            assertEquals(true, false)
        }
    }

    @Test
    fun cropBitmap(){
        var bitmap = getBitmap()
        if(bitmap != cropBitmapByPercent(bitmap, 32F/100F)){
            assertEquals(true, true)
        } else {
            assertEquals(true, false)
        }
    }

    @Test
    fun HTTPRequest(){
    kotlin.run {
        val client = OkHttpClient()
        val request = Request.Builder()
            //Have to use a header to access private JSON bin.
            .url("https://i.picsum.photos/id/90/200/300.jpg")
            .get()
            .build()
        val result: String? = client.newCall(request).execute().body?.string()
        if(result != null){
            print(result)
            assertEquals(true, true)
        } else {
            assertEquals(true, false)
        }
    }

    }

    @Test
    fun ImageDownloadAndDecode(){
        kotlin.run {
            val client = OkHttpClient()
            var request = Request.Builder()
                .url("https://i.picsum.photos/id/90/200/300.jpg")
                .get()
                .build()
            var stream = client.newCall(request).execute().body?.byteStream()
            var buffer = BitmapFactory.decodeStream(stream)
            if(buffer.width == 200){
                assertEquals(true, true)
            } else {
                assertEquals(true, false)
            }
        }

    }

    @Test
    fun QuoteEndPointParse(){
        kotlin.run {
            val SECRET_KEY = "\$2b\$10\$MHRSGH9Nf1VU6xm3l7s63eNu5rM5Q8Q6XMyl/Pw8prN09VmCB.fFO"
            val FETCH_URL = "https://api.jsonbin.io/b/5eb56df4a47fdd6af15fb92c"
            val client = OkHttpClient()
            val request = Request.Builder()
                //Have to use a header to access private JSON bin.
                .header("secret-key", SECRET_KEY)
                .url(FETCH_URL)
                .get()
                .build()
            val result: String? = client.newCall(request).execute().body?.string()
            var quotes : List<QuoteData> = parseJson(result)
            if(quotes.size > 1){
                assertEquals(true, true)
            } else {
                assertEquals(true, false)
            }
        }
    }

    @Test
    fun QuoteEndPoint(){
        kotlin.run {
            val SECRET_KEY = "\$2b\$10\$MHRSGH9Nf1VU6xm3l7s63eNu5rM5Q8Q6XMyl/Pw8prN09VmCB.fFO"
            val FETCH_URL = "https://api.jsonbin.io/b/5eb56df4a47fdd6af15fb92c"
            val client = OkHttpClient()
            val request = Request.Builder()
                //Have to use a header to access private JSON bin.
                .header("secret-key", SECRET_KEY)
                .url(FETCH_URL)
                .get()
                .build()
            val result: String? = client.newCall(request).execute().body?.string()
            if(result != null){
                assertEquals(true, true)
            } else {
                assertEquals(true, false)
            }
        }
    }

    @Test
    fun ApiKey(){
        kotlin.run {
            val SECRET_KEY = "\$2b\$10\$MHRSGH9Nf1VU6xm3l7s63eNu5rM5Q8Q6XMyl/Pw8prN09VmCB.fFO"
            val FETCH_URL = "https://api.jsonbin.io/b/5eb56df4a47fdd6af15fb92c"
            val client = OkHttpClient()
            val request = Request.Builder()
                //Have to use a header to access private JSON bin.
                .header("secret-key", SECRET_KEY)
                .url(FETCH_URL)
                .get()
                .build()
            val result: String? = client.newCall(request).execute().body?.string()
            var quotes : List<QuoteData> = parseJson(result)
            if(quotes.size > 1){
                assertEquals(true, true)
            } else {
                assertEquals(true, false)
            }
        }
    }

    @Test
    fun writeToSD(){
        var bitmap = getBitmap()
        try {
            saveImageToExternalStorage(bitmap)
            assertEquals(true, true)
        } catch (e : Exception){
            assertEquals(true, false)
        }
    }




    private fun saveImageToExternalStorage(bitmap:Bitmap?) {
        // Get the external storage directory path
        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!path.exists()) {
            path.mkdirs()
        }
        // Create a file to save the image
        val file = File(path, "${UUID.randomUUID()}.jpg")
            // Get the file output stream
            val stream: OutputStream = FileOutputStream(file)
            // Compress the bitmap
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            // Flush the output stream
            stream.flush()
            // Close the output stream
            stream.close()
    }


    private fun parseJson(json: String?): List<QuoteData> {
        try {
            var Quotes = mutableListOf<QuoteData>()
            if (json == null) return Quotes
            val jsonArray = JSONArray(json)
            var idx = 0
            while (idx < jsonArray.length()) {
                var jsonObj = jsonArray.getJSONObject(idx)
                val Quote = QuoteData(
                    quote_id = jsonObj.getString("quote_id").toInt(),
                    quote_content = jsonObj.getString("quote_contents"),
                    quote_author = jsonObj.getString("quote_author")
                )
                Quotes.add(Quote)
                idx++
            }
            return Quotes
        } catch (e : Exception){
            //If this fails we return an exception and return an error quote
            var quotes = mutableListOf<QuoteData>()
            quotes.add(QuoteData(0, "Something went wrong fetching quotes", ""))
            Log.e(MainActivity.TAG, "Error : ${e.message}")
            throw e
            return quotes
        }

    }

    private fun Bitmap.rotate(degrees: Float): Bitmap {
        val matrix = Matrix().apply { postRotate(degrees) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    private fun Bitmap.monochrome(): Bitmap {
        //Canvas needs draws colors described by the paint onto a bitmap
        val bmpMonochrome = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bmpMonochrome)
        val matrx = ColorMatrix()
        matrx.setSaturation(0f)
        val paint = Paint()
        paint.setColorFilter(ColorMatrixColorFilter(matrx))
        canvas.drawBitmap(this, 0f, 0f, paint)
        return bmpMonochrome
    }


    private fun changeBitmapColorSpace(bmpOriginal: Bitmap?, bitmapcfg : Bitmap.Config): Bitmap? {
        //Reassigns color space using canvas with Bitmap.Configs
        val height: Int? = bmpOriginal?.height
        val width: Int? = bmpOriginal?.width
        if (width != null && height != null){
            val bmpGrayscale = Bitmap.createBitmap(width, height, bitmapcfg)
            val canvas = Canvas(bmpGrayscale)
            val paint = Paint()
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            val colorMatrixFilter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = colorMatrixFilter
            if (bmpOriginal != null) {
                canvas.drawBitmap(bmpOriginal, 0F, 0F, paint)
            }
            return bmpGrayscale
        } else return bmpOriginal
    }

    private fun changeBitmapColor(bmpOriginal: Bitmap?, color: Int): Bitmap? {
        //Reassigns colors using an int changed to hex.
        val resultBitmap = bmpOriginal?.copy(bmpOriginal?.config, true)
        if (resultBitmap != null) {
            val paint = Paint()
            val filter: ColorFilter = LightingColorFilter(color, 1)
            paint.colorFilter = filter
            val canvas = Canvas(resultBitmap)
            canvas.drawBitmap(resultBitmap, 0F, 0F, paint)
            return resultBitmap
        } else return bmpOriginal
    }

    private fun cropBitmapByPercent(bmpOriginal: Bitmap?, percent : Float): Bitmap? {
        //Resizes image by a given percent value.
        val height: Int? = bmpOriginal?.height?.times(percent)?.toInt()
        val width : Int? = bmpOriginal?.width?.times(percent)?.toInt()
        val resultBitmap = bmpOriginal?.copy(bmpOriginal?.config, true)
        if(resultBitmap != null && height != null && width != null){
            var resizedbitmap1 = Bitmap.createBitmap(resultBitmap, 0, 0, height, width)
            return resizedbitmap1
        } else {
            return null
        }
    }
    
}
