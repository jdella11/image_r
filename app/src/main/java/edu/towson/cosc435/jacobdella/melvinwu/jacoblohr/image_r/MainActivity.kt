package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.graphics.Bitmap
import android.media.Image
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.database.SettingsDataDBRepo
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.database.UriDataDBRepo
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.IImageRController
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.ISettingsRepository
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.IUriRepository
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.QuoteData
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.SettingsData
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.UriData
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.networking.IImagerRAPI
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.networking.ImageR_API
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.services.ImageDownloadService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class MainActivity : AppCompatActivity() , IImageRController{
    override lateinit var uriDataRepo: IUriRepository
    override var bitmapToEdit: Bitmap? = null
    override var bitmapBase: Bitmap? = null
    override lateinit var settingsDataRepo : ISettingsRepository
    override lateinit var imagerAPI: IImagerRAPI
    override var quotesRepo: MutableList<QuoteData> = mutableListOf()
    override val coroutineContext: CoroutineContext
        get() = lifecycleScope.coroutineContext

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        uriDataRepo = UriDataDBRepo(this)
        settingsDataRepo = SettingsDataDBRepo(this)
        imagerAPI = ImageR_API(this)
        //Launch the API to fetch quotes
            launch {
                var quotes = fetchQuotesList()
                for (quote in quotes) {
                    quotesRepo.add(quote)
                }

            }

        //Gives the API just enough time to load the quotes
        launch(Dispatchers.Main){
            delay(500)
            setContentView(R.layout.activity_main)
        }


    }

    //Fetchs the quotes asynchronously
    override suspend fun fetchQuotesList(): List<QuoteData> {
        return imagerAPI.fetchQuotesAsync().await()
    }

    //Starts the image download service
    override fun startImageDownloadService(){
        val scheduler = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        val jobinfo = JobInfo.Builder(JOB_ID, ComponentName(this, ImageDownloadService::class.java.name))
        jobinfo.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
        scheduler.schedule(jobinfo.setOverrideDeadline(10 * 1000).build())

    }

    //Resets the editing bitmap back to before edits were applied
    override fun resetImage() {
        bitmapToEdit = bitmapBase
    }


    override fun getSettingsData(idx: Int): SettingsData {
        return settingsDataRepo.getSettingsData(idx)
    }

    //Toggles a particular index on the settings data.
    override fun toggleSettingData (idx: Int){
        var settingsToggle = settingsDataRepo.getSettingsData(idx)
        settingsToggle.isChecked = !settingsToggle.isChecked
        settingsDataRepo.replaceSettingsValue(idx, settingsToggle)
    }

    //Gets  a particular index of UriData.
    override fun getUriData(idx: Int): UriData {
        return uriDataRepo.getUriData(idx)
    }

    //Adds a particular piece of UriData.
    override fun addUriData(uridata: UriData) {
        uriDataRepo.addUriData(uridata)
    }

    override fun removeUriData(idx: Int) {
        val current = uriDataRepo.getUriData(idx)
        uriDataRepo.removeUriData(current)
    }

    //Runs grabbing the bitmap in the background.
    override suspend fun setEditBitmap(uri : Uri) {
        val uriBitmap: Bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
        bitmapBase = uriBitmap
        bitmapToEdit = uriBitmap
    }

    //Nav graph launch edit settings page
    override fun openSettingsPage(){
        findNavController(R.id.nav_graph)
            .navigate(R.id.action_imageRSplash_to_imageRSettings2)
    }

    //Nav graph launch edit photo page
    override fun editPhotoPage() {
        findNavController(R.id.nav_graph)
            .navigate(R.id.action_imageRSplash_to_imageREdit)
    }


    override fun getUriDataSize(): Int {
       return uriDataRepo.getSize()
    }

    //Depending on which settings are checked certain things will occur on stop of the application
    override fun onStop() {
        super.onStop()
        if(settingsDataRepo.getSettingsData(1).isChecked){
            uriDataRepo.removeAllUriData()
        }

        if(settingsDataRepo.getSettingsData(2).isChecked){
            settingsDataRepo.resetAllSettings()
        }
    }




    companion object {
        val TAG = MainActivity::class.java.simpleName
        val JOB_ID = 1
        val ACTIONOK = "edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.MainActivity"

    }

}