package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.database

import androidx.room.*
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.SettingsData
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.UriData
import java.util.*

//SettingsData Data Access Object
@Dao
interface SettingsDataDAO {
    @Insert
    fun addSettingsData(settingEntry: SettingsData)

    @Update
    fun editSettingData(settingEntry: SettingsData)

    //Queries everything from the database
    @Query("select uuid, value, is_checked from SettingsData")
    fun getAllSettings(): List<SettingsData>

    //Deletes everything from the database
    @Query("DELETE FROM SettingsData")
    fun purgeAllSettingsDB()
}

class SettingsConverters {
    //Converts UUID to string and vice versa
    @TypeConverter
    fun fromString(uuid: String): UUID {
        return UUID.fromString(uuid)
    }

    @TypeConverter
    fun toString(uuid: UUID): String {
        return uuid.toString()
    }
}

@Database(entities = [SettingsData::class], version = 1, exportSchema = false)
@TypeConverters(SettingsConverters::class)
abstract class SettingsDataDB : RoomDatabase() {
    abstract fun settingsdataDAO(): SettingsDataDAO
}