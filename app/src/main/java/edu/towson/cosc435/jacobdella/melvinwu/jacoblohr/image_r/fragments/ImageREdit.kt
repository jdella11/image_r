package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.fragments

import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.R
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.IImageRController
import kotlinx.android.synthetic.main.fragment_image_r_edit.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*



/**
 * This is the image editing fragment, the business end of the application.
 */
class ImageREdit : Fragment() {
    //Instantiate our controller.
    private lateinit var imageRcontroller : IImageRController
    private val filepath = "ImageR"
    internal var myExternalFile: File? = null

    //Check if our controller is of the right type.
    override fun onAttach(context: Context) {
        super.onAttach(context)
        when (context) {
            is IImageRController -> imageRcontroller = context
            else -> throw Exception("IImage_RController Expected")
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_r_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editImageView.setImageBitmap(imageRcontroller.bitmapToEdit)
        imageSaveBtn.setOnClickListener{saveImageToExternalStorage(imageRcontroller.bitmapToEdit)}

        rotateBtn.setOnClickListener{
            //Rotates an image 90 degrees every time it is clicked and displays toast
            imageRcontroller.bitmapToEdit = imageRcontroller.bitmapToEdit?.rotate(90F)
            editImageView.setImageBitmap(imageRcontroller.bitmapToEdit)
            Toast.makeText(view?.context, "Image successfully rotated", Toast.LENGTH_SHORT).show()
        }

        resizeBtn.setOnClickListener{
            //Resizes an image within a given range of pixels every time it is clicked and displays toast
            try {
                if((txtBoxXRes.text.toString().toInt() > 100 && txtBoxYRes.text.toString().toInt() > 100) &&
                    (txtBoxXRes.text.toString().toInt() < 2000 && txtBoxYRes.text.toString().toInt() < 2000)){
                    imageRcontroller.bitmapToEdit = imageRcontroller.bitmapToEdit?.resize(txtBoxXRes.text.toString().toInt(), txtBoxYRes.text.toString().toInt())
                    editImageView.setImageBitmap(imageRcontroller.bitmapToEdit)
                    Toast.makeText(view?.context, "Image successfully resized", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(view?.context, "Invalid parameters", Toast.LENGTH_SHORT).show()
                }
            } catch (e : Exception){
                Toast.makeText(view?.context, "Invalid parameters", Toast.LENGTH_SHORT).show()
            }

        }

        recolorBtn.setOnClickListener{
            //Recolors an image within a given hex color range every time it is clicked and displays toast
            try{
                if(recolorValueTxt.text.toString().toInt() in -50000..50000){
                    imageRcontroller.bitmapToEdit = changeBitmapColor(imageRcontroller.bitmapToEdit, recolorValueTxt.text.toString().toInt()*1000)
                    editImageView.setImageBitmap(imageRcontroller.bitmapToEdit)
                    Toast.makeText(view?.context, "Image successfully recolored", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(view?.context, "Invalid parameters", Toast.LENGTH_SHORT).show()
                }
            } catch (e : Exception){
                Toast.makeText(view?.context, "Invalid parameters", Toast.LENGTH_SHORT).show()
            }

        }
        //Crops an image with to certain percent of it's original size.
        cropBtn.setOnClickListener {
            try{
                if(percentText.text.toString().toFloat() in 1.0..200.0){
                    imageRcontroller.bitmapToEdit = cropBitmapByPercent(imageRcontroller.bitmapToEdit, (percentText.text.toString().toFloat()/100))
                    editImageView.setImageBitmap(imageRcontroller.bitmapToEdit)
                    Toast.makeText(view?.context, "Image successfully resized by percent", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(view?.context, "Invalid parameters", Toast.LENGTH_SHORT).show()
                }
            } catch (e : Exception){
                Toast.makeText(view?.context, "Invalid parameters", Toast.LENGTH_SHORT).show()
            }
        }


        undoEditBtn.setOnClickListener{
            //Resets bitmap to before edits were done.
             imageRcontroller.resetImage()
             editImageView.setImageBitmap(imageRcontroller.bitmapToEdit)
             Toast.makeText(view?.context, "Image edits successfully undone", Toast.LENGTH_SHORT).show()
        }

        convertToBWBtn.setOnClickListener{
            //Reassigns bitmap color space to monochrome
            imageRcontroller.bitmapToEdit = changeBitmapColorSpace(imageRcontroller.bitmapToEdit, Bitmap.Config.ARGB_8888)
            editImageView.setImageBitmap(imageRcontroller.bitmapToEdit)
            Toast.makeText(view?.context, "Image successfully converted to monochrome", Toast.LENGTH_SHORT).show()
        }

    }

    private fun saveImageToExternalStorage(bitmap:Bitmap?) {
            // Get the external storage directory path
            val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            if (!path.exists()) {
                path.mkdirs()
            }
            // Create a file to save the image
            val file = File(path, "${UUID.randomUUID()}.jpg")
            try {
                // Get the file output stream
                val stream: OutputStream = FileOutputStream(file)
                // Compress the bitmap
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                // Flush the output stream
                stream.flush()
                // Close the output stream
                stream.close()
                Toast.makeText(view?.context, "Image successfully saved", Toast.LENGTH_SHORT).show()

            } catch (e: IOException) { // Catch the exception
                Toast.makeText(view?.context, "Image failed to saved", Toast.LENGTH_SHORT).show()
                e.printStackTrace()
            }
        }

    //Bitmap operations
    private fun Bitmap.resize(XRes : Int, YRes : Int) : Bitmap {
        return Bitmap.createScaledBitmap(this, 1 * XRes, 1 * YRes, true)
    }

    private fun Bitmap.rotate(degrees: Float): Bitmap {
        val matrix = Matrix().apply { postRotate(degrees) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    private fun Bitmap.monochrome():Bitmap{
        //Canvas needs draws colors described by the paint onto a bitmap
        val bmpMonochrome = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bmpMonochrome)
        val matrx = ColorMatrix()
        matrx.setSaturation(0f)
        val paint = Paint()
        paint.setColorFilter(ColorMatrixColorFilter(matrx))
        canvas.drawBitmap(this, 0f, 0f, paint)
        return bmpMonochrome
    }


    private fun changeBitmapColorSpace(bmpOriginal: Bitmap?, bitmapcfg : Bitmap.Config): Bitmap? {
        //Reassigns color space using canvas with Bitmap.Configs
        val height: Int? = bmpOriginal?.height
        val width: Int? = bmpOriginal?.width
        if (width != null && height != null){
            val bmpGrayscale = Bitmap.createBitmap(width, height, bitmapcfg)
            val canvas = Canvas(bmpGrayscale)
            val paint = Paint()
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            val colorMatrixFilter = ColorMatrixColorFilter(colorMatrix)
            paint.colorFilter = colorMatrixFilter
            if (bmpOriginal != null) {
                canvas.drawBitmap(bmpOriginal, 0F, 0F, paint)
            }
            return bmpGrayscale
        } else return bmpOriginal
    }

    private fun changeBitmapColor(bmpOriginal: Bitmap?, color: Int): Bitmap? {
        //Reassigns colors using an int changed to hex.
        val resultBitmap = bmpOriginal?.copy(bmpOriginal?.config, true)
        if (resultBitmap != null) {
            val paint = Paint()
            val filter: ColorFilter = LightingColorFilter(color, 1)
            paint.colorFilter = filter
            val canvas = Canvas(resultBitmap)
            canvas.drawBitmap(resultBitmap, 0F, 0F, paint)
            return resultBitmap
        } else return bmpOriginal
    }

    private fun cropBitmapByPercent(bmpOriginal: Bitmap?, percent : Float): Bitmap? {
      //Resizes image by a given percent value.
        val height: Int? = bmpOriginal?.height?.times(percent)?.toInt()
        val width : Int? = bmpOriginal?.width?.times(percent)?.toInt()
        val resultBitmap = bmpOriginal?.copy(bmpOriginal?.config, true)
    if(resultBitmap != null && height != null && width != null){
        var resizedbitmap1 = Bitmap.createBitmap(resultBitmap, 0, 0, height, width)
        return resizedbitmap1
    } else {
        return null
    }
    }



}
