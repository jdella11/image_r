package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.aggregators

import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.ISettingsRepository
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.SettingsData
import java.util.*


//Base settings implementation of Settings Repository Interface
class SettingsDataRepository : ISettingsRepository {
    private var settingsRepo : MutableList<SettingsData> = mutableListOf()

    init{
        //Initialize the settings repo with base values.
        settingsRepo.add(SettingsData(UUID.randomUUID(), true, 0.0))
        settingsRepo.add(SettingsData(UUID.randomUUID(), false, 0.0))
        settingsRepo.add(SettingsData(UUID.randomUUID(), false, 0.0))
    }


    override fun getSettingsList(): List<SettingsData> {
        return settingsRepo
    }

    override fun resetAllSettings() {
        settingsRepo.clear()
    }

    override fun getSettingsData(idx: Int): SettingsData {
        return settingsRepo[idx]
    }

    override fun replaceSettingsValue(idx: Int, settingsData: SettingsData) {
        if(idx >= settingsRepo.size) throw Exception("Outside of bounds")
        settingsRepo[idx] = settingsData
    }

    override fun addSettingsData(settingsData: SettingsData) {
        settingsRepo.add(settingsData)
    }

    override fun getSize(): Int {
        return settingsRepo.size
    }

}
