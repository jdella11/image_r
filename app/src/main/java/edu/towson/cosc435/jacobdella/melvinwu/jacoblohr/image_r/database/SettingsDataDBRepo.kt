package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.database

import android.content.Context
import androidx.room.Room
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces.ISettingsRepository
import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.SettingsData
import java.util.*

class SettingsDataDBRepo (ctx : Context) : ISettingsRepository {
    //Room database object for settings database
    private var settingsList: MutableList<SettingsData> = mutableListOf()
    private var db: SettingsDataDB

    //Database is very small in size so main thread queries won't impact performance.
    init{
        db = Room.databaseBuilder(
            ctx,
            SettingsDataDB::class.java,
            "settingsData.db"
        ).allowMainThreadQueries().build()
    }


    //Grabs settings from room database
    private fun refreshSettingsList() {
        settingsList.clear()
        val settings = db.settingsdataDAO().getAllSettings()
        if(settings.isEmpty()){
            resetAllSettings()
        }
        settingsList.addAll(settings)
    }
    //Reset all settings with default values.
    override fun resetAllSettings() {
        db.settingsdataDAO().purgeAllSettingsDB()
        db.settingsdataDAO().addSettingsData(SettingsData(UUID.randomUUID(), true, 0.0))
        db.settingsdataDAO().addSettingsData(SettingsData(UUID.randomUUID(), false, 0.0))
        db.settingsdataDAO().addSettingsData(SettingsData(UUID.randomUUID(), false, 0.0))
        refreshSettingsList()
    }

    override fun getSettingsData(idx: Int): SettingsData {
        return settingsList[idx]
    }

    override fun getSettingsList(): List<SettingsData> {
        if(settingsList.size == 0){
            refreshSettingsList()
        }
        return settingsList
    }

    override fun replaceSettingsValue(idx : Int, settingsData: SettingsData) {
        db.settingsdataDAO().editSettingData(settingsData)
    }

    override fun getSize(): Int {
        return settingsList.size
    }

    override fun addSettingsData(settingsData: SettingsData){
        db.settingsdataDAO().addSettingsData(settingsData)
    }


}