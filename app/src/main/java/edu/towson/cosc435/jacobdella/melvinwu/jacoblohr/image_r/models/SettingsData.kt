package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models

import android.graphics.Bitmap
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

//Contains data for the SettingsData also a room entity
@Entity
data class SettingsData (
    @PrimaryKey
    val uuid : UUID,
    @ColumnInfo(name = "is_checked")
    var isChecked : Boolean,
    @ColumnInfo(name = "value")
    var value : Double
)
