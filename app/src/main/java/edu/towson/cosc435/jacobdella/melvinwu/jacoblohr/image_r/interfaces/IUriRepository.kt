package edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.interfaces


import edu.towson.cosc435.jacobdella.melvinwu.jacoblohr.image_r.models.UriData
/**
 * This is the interface to control the UriDataStorage.
 */
interface IUriRepository {
    fun getSize(): Int
    fun getUriData(index: Int): UriData
    fun getUriDataList(): List<UriData>
    fun removeUriData(uriData: UriData)
    fun removeAllUriData()
    fun addUriData(uriData: UriData)
    fun replaceUriData(idx: Int, uriData: UriData)
}